/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bites;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author hergusalap
 */

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author hergusalap
 */
public class DecimalBinario {
    
    public void convierte (int decimal){
        Stack<String> pila = new Stack<String>();        
        int dec;
        int exp = 0;
        int residuo = 0;
        double binario = 0;
        while (decimal != 0) {
            residuo = decimal % 2;
            exp++;
            pila.push(Integer.toString(residuo));
            decimal = decimal / 2;
        }
       // System.out.print("El número en binario es ");
        
        while (!pila.empty()) {
            System.out.print(pila.pop());
        }
        
        
    }
    public static void main(String[] args) {
        DecimalBinario db=new DecimalBinario();
        int decimal;
        Scanner lee = new Scanner(System.in);
        System.out.println("Introduce numero en decimal: ");
        decimal = lee.nextInt();
        db.convierte(decimal);
        
        
    }
}
